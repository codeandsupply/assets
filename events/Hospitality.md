Checklist for Hospitable Events

Starting because of 
[this article by Coral Sheldon-Hess](http://www.sheldon-hess.org/coral/2016/06/hospitality-good-ux-for-groups/).

Will use Coral's list as a base and add C&S points to ones Coral does not already have.

# Venue

- [ ] Ability to get to the event space without using stairs
- [ ] Has a gender neutral restroom
- [ ] Seating
- [ ] Wide aisles and places for wheelchairs
- [ ] Soundsystem to amplify voice (for hard of hearing)
- [ ] Public transit nearby (500m)
- [ ] C&S Logo visible from across the street or in a passing car (someday sandwich boards)
- [ ] Signage at each hallway turn or "fork in the road" once inside

# Marketing

- [ ] Echo all hospitality checklist items on the web somewhere

# Logistics communication

- [ ] Schedule posted online (minimum: start time and end time)
- [ ] Printed schedules for structured events. Enough for 60% of registered attendees
- [ ] Explain how to register
- [ ] Send email three days prior to an event with all of the info in this checklist
- [ ] Reminder of conduct policies during all announcements

# Attendee management

- [ ] Conduct policy clearly displayed at entrances or on ticket
- [ ] Identified duty officer (that is NOT the event host) for events over 20 attendees
- [ ] Identified greeter. Duties: welcome people, ask name, direct to seating, encourage meeting someone

# Registration

- [ ] Ask for accessibilty accomodations (give examples: Wheel chair accessibility, no stairs, large print, a quiet space, etc.)
- [ ] Ask for name as a single text field
- [ ] Ask for dietary restrictions
- [ ] Require agreeing to conduct policy to register
- [ ] Send confirmation and reminders as event approaches

# Food

- [ ] Label food and ingredients
- [ ] Communicate food options on the day before

# Presentation

- [ ] No Q&A



More sources:
[Venue accessiblity](http://www.scottishhealthcouncil.org/patient__public_participation/participation_toolkit/accessibility_checklist.aspx#.V-coTKOZO34)